const zh = {
    header:'头部',
    footer:'尾部',
    aside:'侧边栏',
    home:'首页',
    detail:'详情页',
    toDetail:'跳转详情页'
}

export default zh