import Vue from 'vue'
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);
//引入语言配置文件
import zh from './zh'
import en from './en'

const i18n = new VueI18n({
    locale: localStorage.getItem('lang') || 'zh',
    messages:{
        'zh':zh,
        'en':en
    }
})

export default i18n
