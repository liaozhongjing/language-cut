import Vue from 'vue'
import App from './App.vue'
import router from './router'
import i18n from './lang'
import {Autocomplete} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

// 图片滑动验证库
import slideVerify from 'vue-monoplasty-slide-verify'
Vue.use(slideVerify)


Vue.use(Autocomplete)

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
