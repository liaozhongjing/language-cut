export default {
    props:{
        list:{
            type:Array,
            default:()=>[]
        }
    },
    render(h){
        // 使用js原生的if/else和map来实现v-if和v-for
        if(this.list.length > 0) {
            return h('ul',this.list.map(item=>{
                return h('li',item.name)
            }))
        }else {
            return h('div','No List Found')
        }
    }
}