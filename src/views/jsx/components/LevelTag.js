export default {
    data() {
        return {
            flag: true
        }
    },
    props: {
        level: {
            type: String,
            default: '1'
        }
    },
    render: function (h) {
        return h('h' + this.level, {
            //类似于v-bind: class
            class: {
                foo: this.flag,
                go: false
            },
            //类似于v-bind:style
            style: {
                color: 'red',
                fontSize: '15px'
            },
            //普通的html属性
            attrs: {
                id: 'id'
            },
            // DOM property
            domProps: {
                // innerHTML: 'baz'
            },
            on: {
                click: this.clickHandler
            },
            // 仅用于组件，用于监听原生事件，而不是组件内部使用
            // `vm.$emit` 触发的事件。
            // nativeOn: {
            //     // click: this.nativeClickHandler
            // },
            // 作用域插槽的格式为
            // { name: props => VNode | Array<VNode> }
            // scopedSlots: {
            //     name: props => createElement('span', props.text)
            // },
        }, [h('a',this.$slots.default),this.$scopedSlots.name({level:this.level})])
    },
    methods: {
        clickHandler() {
            alert('点击')
        },
        nativeClickHandler() {
            alert('原生事件')
        }
    }
}