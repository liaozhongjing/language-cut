export default {
    props:{
        value:{
            type:String,
            default:''
        }
    },
    render(h){
        return h('input',{
            domProps:{
                value:this.value
            },
            on:{
                input:(e)=>{
                    this.$emit('input',e.target.value)
                }
            }
        })
    }
}