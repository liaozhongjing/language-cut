import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../layout'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/home',
    component: Layout,
    children:[
      {
        path:'home',
        component:()=> import ('../views/Home.vue')
      },
      {
        path:'detail',
        component:()=> import ('../views/Detail.vue')
      },
      {
        path:'slide_verify',
        component:()=> import ('../views/Slide_verify.vue')
      },
      {
        path:'jsx',
        component:()=> import ('../views/jsx/index.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
